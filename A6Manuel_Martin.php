<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Manuel Martín</title>
</head>
<body>

    <h1>¡Bienvenido a Adivina el número!</h1>
    
    <?php

    $numero = 113;

    $guessNumber = $_GET['guess'] ?? null;

    if(!isset($guessNumber)){

        echo '<h2>No has introducido ningún parámetro</h2>';

    }elseif(!is_numeric($guessNumber)){

        echo '<h2>El parámetro no es un número!</h2>';

    }elseif(isset($guessNumber) && $guessNumber < $numero) {

        echo '<h2>El número que has pasado es más bajo!</h2>';

    }elseif(isset($guessNumber) && $guessNumber > $numero){

        echo '<h2>El número que has pasado es más alto!</h2>';

    }elseif(isset($guessNumber) && $guessNumber == $numero){

        echo "<h2>¡Muy bien! el número correcto es $numero!</h2>";
    }

    


    ?>    


</body>
</html>