<?php

echo '<h1>MD5 Cracker</h1>';
$diccionario_claves = array();

for ($i=0; $i <= 9999 ; $i++) { 
    $diccionario_claves[md5(vsprintf("%04d", explode('-',"$i")))] = vsprintf("%04d", explode('-',"$i"));
}



$check = $_GET["md5"] ?? null;




//Aproximación con array_key_exists:

//$result = array_key_exists($check, $diccionario_claves) ? print_r("El pin es $diccionario_claves[$check]") : print_r('No hemos encontrado el PIN');

//Aproximación con foreach:

$pin = '';
$iterations = 0;
$foundKeyIteration = 0;
$showResults = array();   
$time = microtime(true);

foreach($diccionario_claves as $key => $value){
    if($check != $key){
        $iterations++;
        
    }
    if($iterations <= 15){
        $showResults[$key] = $value;
    }
    if($check == $key){
        $pin = $value;
        $foundKeyIteration = $iterations;
    }
}

// Tiempo transcurrido: tiempo del actual microtime - tiempo del primer microtime. 
$time_elapsed = microtime(true) -$time;

?>
<p>Debug Output:</p>
<table>
<?php
foreach($showResults as $key => $value){?>
    <tr>
        <td>
            <?php echo $key;?>
        </td>
        <td>
            <?php echo $value;?>
        </td>
    </tr>
<?php   
}
?>
</table>

<?php
echo '<p>Las iteraciones totales han sido:'. ($foundKeyIteration!=0 ? $foundKeyIteration : $iterations).'</p>';
echo "<p>Tiempo transcurrido: $time_elapsed</p>";

$result = 'PIN: '.($pin!='' ? $pin : "No encontrado\r\n");
echo $result;

?>

<form action="index.php" method="get">
<input type="text" name="md5" size="60" />
<input type="submit" value="Crack MD5"/>
</form>