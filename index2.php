<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Manuel Martin Pérez</title>
</head>
<body>
    <h1>Manuel Martín Pérez Request/Reponse</h1>
    <pre>
        ASCII ART:

        mm             mm
        mm             mm
        mm mm       mm mm
        mm    mm  mm   mm
        mm      mm     mm
    </pre>
    <p>El cálculo del código hash SHA256 de Manuel Martín es: <?php echo  hash('sha256','Manuel Martín Pérez');?></p>

    <p><a href="check.php">Pincha aquí para comprobar los error setting</a></p>
    <p><a href="fail.php">Pincha aquí para causar un traceback</a></p> 
</body>
</html>